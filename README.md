# Downloading both NGINX and installing WildFly 🪰

This is a document explaining the set up of Wildfly and Nginx on an Ubuntu Instance.

### Instance Setup ☁️
- Log into AWS and navigate to EC2. Create an instance with the following:

```bash
AMI: Ubuntu Server 18.04 LTS
Instance Type: t2.micro, 1vCPU, 1GiB Memory
```

Make sure the 'Auto- assign Public IP' is on **enable**

Leave the Storage as it is- this is sufficient for the installation of both Wildfly and Nginx.

Add any necessary tags e.g. **Name** and **Projectname**

**"Configure Security Group"** Choose the correct security group for your instance but for this example we are using 'Cohort9-homeips' and 'AllowJenkinsMachines'.

**Key Pair** You need to choose an existing key pair or create a new one. Again for the purpose of this example we are using an existing key called 'Cohort9_shared'

**LAUNCH INSTANCE**

&nbsp;
&nbsp;
&nbsp;

## Installing Wildfly and Nginx through Jenkins
Prerequisites
- You have created the Ubuntu instance following the steps above with thr correct security groups listed.
- Please note that we could not fully automate the end configuration of files so you will need to manually following the instructions to configure them that follow after the installation of both WildFly and NGINX- you will be told when you need to copy and paste information.

- Access to Dynamic DevOps Jenkins server on port 8080 using the correct login credentials
- Access to the ch9_shared.pem security key 

First: Log into Jenkins on http://3.250.191.93:8080/


Next: On the main dashboard, select the job 'Wildfly CD' and navigate to the configuration page. Ensure you go through all the sections and select the correct options. In the 'Build' section of the configuration setting make sure you write the name of the script along with the private IP of the Ubuntu instance and then $KEY so that Jenkins will use the variable which was set earlier to run the script correctly.

This would look like **./Wildflydynamicfinal.sh 172.31.44.127 $KEY**


Click save and press build now.

Once the build has been accepted you will get a slack notification and once the build is complete it will again inform you.

You can then enter the public IP for the Wildfly instance into your url search bar and you should see the NGINX default page.

You can now continue to manually configure this page with Wildfly following the steps further down in this documentation.


&nbsp;

&nbsp;

&nbsp;



### Installing Wildfly and Nginx- manually if needed/wanted

Prerequisites
- You have created the Ubuntu instance following the steps above.

- You have cloned the repository using the command:
```bash
$ git clone git@bitbucket.org:garthj98/wildfly.git
```

Make sure you are located in the correct path of this directory 
```bash
$ cd path/to/repo

# Run the installation script 

$ ./<name/of/script> <public/ip/of/instance>
```

This will then log you into the instance as the necessary variables have been set in the script. You just need to be in the path of the script then paste the public IP for the instance you made.

In the script are the step by step commands to download Wildfly then to install Nginx and configure them so that they present the correct Wildfly homepage without the need to input :8080 at the end of the url. 

&nbsp;
&nbsp;
&nbsp;


## Manually setup NGINX as a reverse proxy for WildFly:

Prerequisites:

- Make sure you ssh back into the VM so that you can manually continue with the next steps.

Once the installation of nginx is completely done via the script you cloned you need to follow these next steps manually to configure Nginx as a reverse proxy for Wildfly.


## Manual Configuration of WildFly

Once Jenkins completes your build you need to go and log back into your AWS instance using the ssh command:

```bash
$ ssh -i path/to/key/ch9_shared.pem ubuntu@<instance_ip>
```

Now you need to configure the following files:
```bash
sudo nano /opt/wildfly/bin/launch.sh
```

when inside this file you need to delete the exisiting code and paste the following:

```bash
#!/bin/sh

if [ "x$WILDFLY_HOME" = "x" ]; then
    WILDFLY_HOME="/opt/wildfly"
fi

if [[ "$1" == "domain" ]]; then
    $WILDFLY_HOME/bin/domain.sh -c $2 -b $3 -bmanagement $4
else
    $WILDFLY_HOME/bin/standalone.sh -c $2 -b $3 -bmanagement $4
fi

```

Save and exit.

```bash
sudo nano /etc/systemd/system/wildfly.service
```

delete the existing content and paste the following:
```bash
[Unit]
Description=The WildFly Application Server
After=syslog.target network.target
Before=httpd.service

[Service]
Environment=LAUNCH_JBOSS_IN_BACKGROUND=1
EnvironmentFile=-/etc/wildfly/wildfly.conf
User=wildfly
LimitNOFILE=102642
PIDFile=/var/run/wildfly/wildfly.pid
ExecStart=/opt/wildfly/bin/launch.sh $WILDFLY_MODE $WILDFLY_CONFIG $WILDFLY_BIND $WILDFLY_CONSOLE_BIND
StandardOutput=null

[Install]
WantedBy=multi-user.target

```

Save and exit.

On your instance run these following commands:
```bash
sudo systemctl daemon-reload

sudo systemctl restart wildfly

# check the status of WildFly and see it is running
sudo systemctl status wildfly

# control + c + enter to exit
```

&nbsp;
&nbsp;
&nbsp;

## Manual configuration of NGINX 

```bash
sudo nano /etc/nginx/sites-available/wildfly.conf
```
remove any contents present and replace it with:


```bash
 
upstream wildfly {
  server 127.0.0.1:8080 weight=100 max_fails=5 fail_timeout=5;
}

server {
  listen          80;
  server_name     <Instance_IP>;

  location / {
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://wildfly/;
  }
}


```




Please note: enter the public IP of the instance where asked '<Instance_IP>'

Save and Exit.

```bash
# run this command to check NGINX is not broken- you should get a success message.
sudo nginx -t

# Finally run the next two commands and you're done.
sudo ln -s /etc/nginx/sites-available/wildfly.conf /etc/nginx/sites-enabled/

sudo systemctl restart nginx
```

Great... you have now installed and configured WildFly with NGINX. If you want to confirm this type the IP of the instance into your web browser and you should see the WildFly default page being hosted by Nginx.

The client's technical consultant Manuel will now take over from here 🚀
