IPADDRESS=$1
KEY=$2

# Log into the remote machine
ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$IPADDRESS << EOF


#necessary updates and installations
sudo apt-get update
sudo apt-get install zip unzip -y
sudo apt-get install default-jdk -y
sudo apt-get upgrade -y
sudo apt-get install nginx -y

# creation of system user to run the service
sudo groupadd -r wildfly 
sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly

# installation of wildfly
wget https://github.com/wildfly/wildfly/releases/download/26.0.1.Final/wildfly-26.0.1.Final.zip
unzip wildfly-26.0.1.Final.zip

# changing ownership of the file
sudo cp -r wildfly-26.0.1.Final /opt/wildfly
sudo chown -RH wildfly: /opt/wildfly

#configuration of wildfly on systemd
sudo mkdir /etc/wildfly
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/

# allow the scripts to run
sudo sh -c "chmod +x /opt/wildfly/bin/*.sh"
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl start wildfly
sudo systemctl enable wildfly
#sudo systemctl status wildfly

#<secret value="YXNzZXNzbWVudDIh" />

# configure wildfly to be accessed from the remote system 
sudo sh -c "cat >/etc/wildfly/wildfly.conf << _END_
# The configuration you want to run
WILDFLY_CONFIG=standalone.xml

# The mode you want to run
WILDFLY_MODE=standalone

# The address to bind to
WILDFLY_BIND=0.0.0.0

WILDFLY_CONSOLE_BIND=0.0.0.0

_END_"

sudo sh -c "cat >/opt/wildfly/bin/launch.sh << _END_
#!/bin/sh

if [ "x$WILDFLY_HOME" = "x" ]; then
    WILDFLY_HOME="/opt/wildfly"
fi

if [[ "$1" == "domain" ]]; then
    $WILDFLY_HOME/bin/domain.sh -c $2 -b $3 -bmanagement $4
else
    $WILDFLY_HOME/bin/standalone.sh -c $2 -b $3 -bmanagement $4
fi

_END_"

sudo sh -c "cat >/etc/systemd/system/wildfly.service << _END_
[Unit]
Description=The WildFly Application Server
After=syslog.target network.target
Before=httpd.service

[Service]
Environment=LAUNCH_JBOSS_IN_BACKGROUND=1
EnvironmentFile=-/etc/wildfly/wildfly.conf
User=wildfly
LimitNOFILE=102642
PIDFile=/var/run/wildfly/wildfly.pid
ExecStart=/opt/wildfly/bin/launch.sh $WILDFLY_MODE $WILDFLY_CONFIG $WILDFLY_BIND $WILDFLY_CONSOLE_BIND
StandardOutput=null

[Install]
WantedBy=multi-user.target
_END_"

sudo mkdir /var/run/wildfly/
#sudo chown wildlfy /var/run/wildfly/
sudo chown wildfly: /var/run/wildfly/

sudo systemctl daemon-reload
sudo systemctl restart wildfly

EOF